//Crear una escena 
//THREE
let escena = new THREE.Scene();
//Crear camara
let camara = new THREE.PerspectiveCamera(100, window.innerWidth / window.innerHeight, 0.1, 1000);

//Crear lienzo (renderer)
let lienzo =  new THREE.WebGLRenderer();
lienzo.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(lienzo.domElement);

//Crear geomtria

let cuboide = new THREE.BoxGeometry();
let material = new THREE.MeshBasicMaterial({color: 0x0B72BE});

//Crear la malla
let miCubo = new THREE.Mesh(cuboide, material);

//Agregar  "la malla" a la escena

escena.add(miCubo);

camara.position.z = 3;

let animar = function(){
    requestAnimationFrame(animar);
    miCubo.rotation.x = miCubo.rotation.x + 0.01;
    miCubo.rotation.y = miCubo.rotation.y + 0.01;

    if(miCubo.position.y < 3 && miCubo.position.x == 0){
        miCubo.position.y = miCubo.position.y +0.01; 
    }else{
        if(miCubo.position.y > 2 && miCubo.position.x < 8){
            miCubo.position.x = miCubo.position.x +0.01;
        }else{
            if(miCubo.position.y > -3 && miCubo.position.x > 7){
                miCubo.position.y = miCubo.position.y -0.01;
            }else{
                if(miCubo.position.x > -7){
                    miCubo.position.x = miCubo.position.x -0.01;
                }else{
                    if(miCubo.position.y < 5){
                        miCubo.position.y = miCubo.position.y +0.01;
                    }
                }
            }
        }
    }

    lienzo.render(escena,camara);
    
}

animar();


